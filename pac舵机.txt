#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

// 创建 PCA9685 驱动实例
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(0x40);

// 定义舵机通道
#define SERVO_CHANNEL_1 0  // 第一个舵机连接到通道 0

// 舵机脉宽范围 (微秒)
#define SERVO_MIN 150  // 舵机最小角度对应的脉宽
#define SERVO_MAX 600  // 舵机最大角度对应的脉宽

// 舵机的实际工作角度范围
#define SERVO1_START_ANGLE 0   // 第一个舵机起始角度
#define SERVO1_END_ANGLE 180   // 第一个舵机结束角度
#define SERVO1_STEP 10         // 每次增加的角度

// 将角度转换为 PCA9685 的脉宽值
int angleToPulse(int angle) {
    return map(angle, 0, 180, SERVO_MIN, SERVO_MAX);
}

// 初始化函数
void setup() {
    Serial.begin(115200); // 初始化串口用于调试
    Serial.println("初始化 PCA9685 驱动板...");

    // 初始化 I2C 总线，指定 SDA 和 SCL 引脚
    Wire.begin(42, 41); // ESP32-S3-CAM 的 SDA 和 SCL 引脚分别为 GPIO42 和 GPIO41

    // 初始化 PCA9685 驱动板
    if (!pwm.begin()) {
        Serial.println("PCA9685 初始化失败，请检查连接！");
        while (1) delay(1000); // 如果初始化失败，进入死循环
    }

    pwm.setPWMFreq(50); // 设置 PWM 频率为 50Hz（舵机标准）
    delay(1000);
    Serial.println("初始化完成！");
}

// 主循环函数
void loop() {
    for (int angle = SERVO1_START_ANGLE; angle <= SERVO1_END_ANGLE; angle += SERVO1_STEP) {
        pwm.setPWM(SERVO_CHANNEL_1, 0, angleToPulse(angle)); // 设置舵机角度
        Serial.print("舵机1当前角度: ");
        Serial.println(angle); // 输出当前角度
        delay(2000); // 延迟2秒
    }

    // 测试完成后停止
    Serial.println("角度测试完成！");
    while (1) {
        delay(1000); // 停止程序
    }
}

// ESP-IDF 的入口函数
extern "C" void app_main() {
    setup(); // 调用初始化函数

    // 主循环
    while (true) {
        loop(); // 调用主循环函数
    }
}